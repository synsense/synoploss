# This is for testing models with the 'smart' rescaling proposed by Rueckauer 2017,
# where the gain for each layer is estimated and used to normalize.
from torch.utils.data import DataLoader
import torch
import numpy as np
from tqdm import tqdm
from torchvision import transforms
from neurovgg import get_neurovgg, get_next_weighted_layer_fanout
from torchvision.datasets import ImageFolder


WEIGHTED_LAYERS = (torch.nn.Conv2d, torch.nn.Linear, )


def rescale_sequential(seq, dataloader, percentile=99.0, max_batches=None, previous_networks=None):
    # Create a function that creates hook functions
    def hook_factory(name):
        def hook(self, input, output):
            if name not in outputs_register.keys():
                outputs_register[name] = []
            outputs_register[name].append(output.data.cpu())
        return hook

    outputs_register = {'_inputs': []}

    for i, layer in enumerate(seq):
        if isinstance(layer, WEIGHTED_LAYERS):
            nextl = i+1 if i+1 < len(seq) else i
            if nextl not in outputs_register.keys():
                seq[nextl].register_forward_hook(hook_factory(nextl))
            if i-1 >= 0 and i-1 not in outputs_register.keys():
                seq[i-1].register_forward_hook(hook_factory(i-1))

    with torch.no_grad():
        for batch_id, sample in enumerate(tqdm(dataloader)):
            data, label = sample
            outputs_register['_inputs'].append(data)
            data = data.cuda()
            if previous_networks is not None:
                for net in previous_networks:
                    data = net(data)
            seq(data)

            if max_batches is not None and batch_id >= max_batches:
                break  # we don't have enough memory to save all outputs

        scales = {}
        for i, (layer, output) in enumerate(outputs_register.items()):
            layer_outputs = torch.stack(output).cpu().numpy().ravel()
            scales[layer] = np.percentile(np.abs(layer_outputs), q=percentile)

        for i, layer in enumerate(seq):
            if isinstance(layer, (torch.nn.Conv2d, torch.nn.Linear)):
                prev_scale = scales[i-1] if i > 0 else scales['_inputs']
                next_scale = scales[i+1] if i+1 < len(seq) else scales[i]
                layer.weight *= prev_scale / next_scale
                layer.bias /= next_scale

    return seq


if __name__ == '__main__':
    # Prepare datasets and dataloaders
    transform = transforms.Compose([
        transforms.Resize((256, 256)),
        transforms.ToTensor()
    ])

    model = get_neurovgg(quantize=False, fanout=False)
    dataset_train = ImageFolder('/home/martino/StorageRoom/datasets/imagenette-320/train/', transform=transform)
    dataloader_train = DataLoader(dataset_train, batch_size=8, shuffle=True)

    # load the model
    model_path = "previous_trainings/vgg_imagenette.pth"
    state_dict = torch.load(model_path)
    model.load_state_dict(state_dict)
    model.eval().cuda()


    PERCENTILE = 95.
    model.classifier = rescale_sequential(model.classifier, dataloader_train, max_batches=20, percentile=PERCENTILE,
                                          previous_networks=(model.features, model.avgpool, lambda x: torch.flatten(x, 1)))
    model.features = rescale_sequential(model.features, dataloader_train, max_batches=20, percentile=PERCENTILE)

    torch.save(model.state_dict(), f'smart_rescaled/vgg_imagenette_gain_rescaled_new{PERCENTILE}.pth')
