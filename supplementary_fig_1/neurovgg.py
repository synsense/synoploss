from torchvision.models import vgg16
from sinabs.layers import NeuromorphicReLU
import torch
from torch import nn
import warnings
import numpy as np


def get_next_weighted_layer_fanout(layer_sequence, layer_id, max_depth=3):
    for i in range(1, max_depth+1):
        if layer_id + i >= len(layer_sequence):
            break

        if isinstance(layer_sequence[i + layer_id], nn.Conv2d):
            nextl = layer_sequence[i + layer_id]
            return i + layer_id, np.product(nextl.kernel_size) * nextl.out_channels

        if isinstance(layer_sequence[i + layer_id], nn.Linear):
            return i + layer_id, layer_sequence[i + layer_id].out_features

    warnings.warn('Could not find next weighted layer for layer ' + str(layer_id))
    return None, None


def convert_relus(model, quantize, fanout):
    # turn the ReLUs in the model into NeuromorphicReLUs
    for i, layer in enumerate(model):
        if isinstance(layer, torch.nn.ReLU):
            next_fanout = get_next_weighted_layer_fanout(model, i)[1] if fanout else 1.
            model[i] = NeuromorphicReLU(quantize=quantize, fanout=next_fanout)
    return model


def get_neurovgg(quantize, fanout):
    # get vgg from torchvision
    neurovgg = vgg16()
    # change last layer to match correct output size
    neurovgg.classifier[6] = torch.nn.Linear(in_features=4096, out_features=10, bias=True)
    # convert relus to neuromorphic
    neurovgg.classifier = convert_relus(neurovgg.classifier, quantize, fanout)
    neurovgg.features = convert_relus(neurovgg.features, quantize, fanout)
    neurovgg.features[29].fanout = 4096 if fanout else 1.
    # transfer to GPU
    neurovgg.cuda()
    return neurovgg
