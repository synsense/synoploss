import torch
from neurovgg import get_neurovgg
from sinabs import SynOpCounter
from tqdm import tqdm
import argparse
from torch.utils.data import DataLoader
from torchvision.datasets import ImageFolder
from torchvision import transforms
from analog_test import test
from numpy import nan

parser = argparse.ArgumentParser()
parser.add_argument('--checkpoint', type=str)
parser.add_argument('--batch_size', type=int, default=32)
parser.add_argument('--quantize', action='store_true')
parser.add_argument('--alpha', type=float, default=0.0)
parser.add_argument('--fanout', action='store_true')
parser.add_argument('--rescale', type=float, default=1.0)
parser.add_argument('--lr', type=float, default=1e-3)
args = parser.parse_args()


transform = transforms.Compose([
    transforms.Resize((256, 256)),
    transforms.ToTensor()
])

neurovgg = get_neurovgg(quantize=args.quantize, fanout=args.fanout)

dataset_train = ImageFolder('/home/martino/StorageRoom/datasets/imagenette-320/train/', transform=transform)
dataloader_train = DataLoader(dataset_train, batch_size=args.batch_size, shuffle=True)

optimizer = torch.optim.SGD(neurovgg.parameters(), lr=args.lr)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.33)
criterion = torch.nn.CrossEntropyLoss()
synopcount = SynOpCounter(neurovgg.modules())

state_dict = torch.load(args.checkpoint)
state_dict['features.0.weight'] *= args.rescale
state_dict['features.0.bias'] *= args.rescale
neurovgg.load_state_dict(state_dict)


for epoch in range(30):
    pbar = tqdm(dataloader_train)
    for (sample, label) in pbar:
        optimizer.zero_grad()

        out = neurovgg(sample.cuda())

        loss1 = criterion(out, label.cuda())
        loss2 = args.alpha * synopcount()
        loss = loss1 + loss2
        if torch.isnan(loss):
            raise Exception('nan')
        loss.backward()
        optimizer.step()

        pbar.set_postfix(crossentropy=loss1.item(), synoploss=loss2.item())

    print(test(neurovgg))
    scheduler.step()

    torch.save(
        neurovgg.state_dict(),
        f'trainings/vgg_imagenette_qtrain{args.quantize}_fanout_{args.fanout}_l1loss{args.alpha}_rescale{args.rescale}.pth'
    )
