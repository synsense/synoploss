from sinabs.from_torch import from_model
from tqdm import tqdm
import torch
from torch.utils.data import DataLoader
from torchvision.datasets import ImageFolder
from torchvision import transforms
from neurovgg import get_neurovgg
import argparse

transform = transforms.Compose([
    transforms.Resize((256, 256)),
    transforms.ToTensor()
])


def spiking_test(model, rescale=1.0, n_dt=1):
    state_dict = torch.load(model)
    state_dict['features.0.weight'] *= rescale
    state_dict['features.0.bias'] *= rescale
    neurovgg = get_neurovgg(quantize=True, fanout=True)
    neurovgg.load_state_dict(state_dict)

    spk_classifier = from_model(neurovgg.classifier, input_shape=(25088, 1, 1), threshold_low=-100, bias_rescaling=n_dt)
    spk_features = from_model(neurovgg.features, input_shape=(1, 256, 256), threshold_low=-100, bias_rescaling=n_dt)

    spk_classifier.eval()
    spk_features.eval()

    dataset_test_spk = ImageFolder('/home/martino/StorageRoom/datasets/imagenette-320/val/', transform=transform)
    dataloader_test_spk = DataLoader(dataset_test_spk, batch_size=1, shuffle=True)

    acc = []
    synops = []
    with torch.no_grad():
        for (sample0, label) in tqdm(dataloader_test_spk):
            sample = sample0.cuda().repeat((n_dt, 1, 1, 1)) / n_dt
            intermediate = spk_features(sample)
            intermediate = neurovgg.avgpool(intermediate)
            intermediate = torch.flatten(intermediate, 1)
            output = spk_classifier(intermediate)

            class_synops = spk_classifier.get_synops(0).SynOps.sum()
            feat_synops = spk_features.get_synops(0).SynOps.sum()
            synops.append(class_synops + feat_synops)

            predicted_label = torch.max(output.sum(0), axis=0).indices
            acc.append((predicted_label.cpu() == label).numpy().mean())

            spk_classifier.reset_states()
            spk_features.reset_states()
    return sum(acc) / len(acc), sum(synops)/len(synops)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('model', type=str, nargs=1)
    parser.add_argument('--rescale', type=float, default=1.0)
    args = parser.parse_args()

    acc, syn = spiking_test(args.model[0], args.rescale)
    print('mean accuracy', acc)
    print('mean synops', syn)
