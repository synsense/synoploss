from spiking_test import spiking_test
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('folder', type=str, nargs=1, default='./trainings/')
parser.add_argument('--rescale', type=float, default=1.0)
args = parser.parse_args()

ndt = 10

results = open(f'results_smartrescale_scale{args.rescale}_ndt{ndt}.txt', 'w')

acc, syn = {}, {}
for f in os.listdir(args.folder[0]):
    fpath = os.path.join(args.folder[0], f)
    acc[f], syn[f] = spiking_test(fpath, args.rescale, n_dt=ndt)
    results.write(f'{f}, {acc[f]}, {syn[f]}\n')

results.close()
