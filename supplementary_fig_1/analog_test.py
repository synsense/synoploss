from torchvision.datasets import ImageFolder
from torch.utils.data import DataLoader
from torchvision import transforms
from sinabs import SynOpCounter
from tqdm import tqdm
import torch
import argparse
from neurovgg import get_neurovgg

transform = transforms.Compose([
    transforms.Resize((256, 256)),
    transforms.ToTensor()
])

dataset_test = ImageFolder('/home/martino/StorageRoom/datasets/imagenette-320/val/', transform=transform)
dataloader_test = DataLoader(dataset_test, batch_size=32, shuffle=True)


def test(network):
    network.eval()

    synopcounter = SynOpCounter(network.modules())

    acc = []
    synops = []
    with torch.no_grad():
        for (sample, label) in tqdm(dataloader_test):
            out = network(sample.cuda())

            predicted_label = torch.max(out, axis=1).indices
            acc.append((predicted_label.cpu() == label).numpy().mean())
            synops.append(synopcounter())

    return sum(acc) / len(acc), sum(synops).item() / len(synops)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('model', type=str, nargs=1)
    parser.add_argument('--rescale', type=float, default=1.0)
    args = parser.parse_args()

    state_dict = torch.load(args.model[0])
    state_dict['features.0.weight'] *= args.rescale
    state_dict['features.0.bias'] *= args.rescale
    neurovgg = get_neurovgg(quantize=False, fanout=False)
    neurovgg.load_state_dict(state_dict)

    print(test(neurovgg))
